from Adafruit_MotorHAT import Adafruit_MotorHAT

class Motor():
    def __init__(self, channel=1, i2c_bus=7):
        self._driver = Adafruit_MotorHAT(i2c_bus=i2c_bus)
        self._motor = self._driver.getMotor(channel)
    
    def _write_value(self, value):
        mapped_value = int(255.0 * value)
        speed = min(max(abs(mapped_value), 0), 255)
        self._motor.setSpeed(speed)
        if mapped_value < 0:
            self._motor.run(Adafruit_MotorHAT.FORWARD)
        else:
            self._motor.run(Adafruit_MotorHAT.BACKWARD)            

    def _release(self):
        self._motor.setSpeed(0)
        self._motor.run(Adafruit_MotorHAT.RELEASE)
    
    def __del__(self):
        self._release()
        
def main():
    # jetbot ai val
    # 1 : fwd
    # -1 : bwd
    
    motor_left = Motor(1)
    motor_right = Motor(2)
    
    val = 0.3
    # motor_right._write_value(val)
    motor_left._write_value(val)
    
    import time
    time.sleep(1)

if __name__ == '__main__' :
    main()    
    
